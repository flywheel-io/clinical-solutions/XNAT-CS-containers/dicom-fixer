FROM python:3.11.5-slim-bookworm
SHELL ["/bin/bash", "-Eeuxo", "pipefail", "-c"]

# enforce language and collation settings
ENV LANG=C.UTF-8
ENV LANGUAGE=en_US
ENV LC_ALL=C.UTF-8

RUN mkdir -p /flywheel/v0

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

RUN apt-get update; \
    apt-get install -yq --no-install-recommends \
        curl

# libpython3.9-dev
RUN apt-get update && apt-get install -y build-essential && \
    curl --proto '=https' --tlsv1.2 -sSf \
    https://sh.rustup.rs | sh -s -- -y \
    && source "$HOME/.cargo/env" 
ENV PATH="/root/.cargo/bin:${PATH}"

# Installing main dependencies
COPY requirements.txt $FLYWHEEL/
RUN pip install --no-cache-dir -r $FLYWHEEL/requirements.txt

# Installing the current project
COPY ./ $FLYWHEEL/

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["python","/flywheel/v0/run.py"]

LABEL org.nrg.commands="[{\"name\": \"fixer\", \"label\": \"fixer\", \"description\": \"Runs DICOM fixer v1.1\", \"version\": \"1.1\", \"schema-version\": \"1.0\", \"image\": \"registry.gitlab.com/flywheel-io/clinical-solutions/xnat-cs-containers/dicom-fixer:1.1\", \"type\": \"docker\", \"command-line\": \"python run.py --input_dir /input/DICOM --output_dir /output #DEBUG# #TRANSFERSYNTAX# #STRICTVALIDATION# #UNIQUE# #FORCE_DECOMPRESS# #SESSION_LABEL# #PROJECT#\", \"override-entrypoint\": true, \"mounts\": [{\"name\": \"in\", \"writable\": false, \"path\": \"/input\"}, {\"name\": \"out\", \"writable\": true, \"path\": \"/output\"}], \"environment-variables\": {}, \"ports\": {}, \"inputs\": [{\"name\": \"debug\", \"description\": \"Include debug statements in the output\", \"type\": \"select-one\", \"default-value\": \"False\", \"required\": false, \"replacement-key\": \"#DEBUG#\", \"command-line-flag\": \"--debug\", \"command-line-separator\": \" \", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"standardize_transfersyntax\", \"description\": \"Whether or not to change TransferSyntaxUID to ExplicitVRLittleEndian\", \"type\": \"select-one\", \"default-value\": \"True\", \"required\": false, \"replacement-key\": \"#TRANSFERSYNTAX#\", \"command-line-flag\": \"--standardize_transfersyntax\", \"command-line-separator\": \" \", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"strict_validation\", \"description\": \"Enforce strict DICOM validation to the DICOM standard\", \"type\": \"select-one\", \"default-value\": \"True\", \"required\": false, \"replacement-key\": \"#STRICTVALIDATION#\", \"command-line-flag\": \"--strict_validation\", \"command-line-separator\": \" \", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"unique\", \"description\": \"Enforce DICOM uniqueness by SOPInstanceUID or file hash. Remove duplicate DICOMs.\", \"type\": \"select-one\", \"default-value\": \"True\", \"required\": false, \"replacement-key\": \"#UNIQUE#\", \"command-line-flag\": \"--unique\", \"command-line-separator\": \" \", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"force_decompress\", \"description\": \"Expert option: Force standardize_transfer_syntax fix even if filesize may be too big for available memory. WARNING: Choosing this option may result in the container failing due to being out of memory.\", \"type\": \"select-one\", \"default-value\": \"False\", \"required\": false, \"replacement-key\": \"#FORCE_DECOMPRESS#\", \"command-line-flag\": \"--force_decompress\", \"command-line-separator\": \" \", \"select-values\": [\"True\", \"False\"]}, {\"name\": \"project\", \"description\": \"XNAT project\", \"type\": \"string\", \"required\": true, \"replacement-key\": \"#PROJECT#\", \"command-line-flag\": \"--projectid\", \"command-line-separator\": \" \", \"select-values\": []}, {\"name\": \"session-label\", \"description\": \"Session Label\", \"type\": \"string\", \"required\": true, \"replacement-key\": \"#SESSION_LABEL#\", \"command-line-flag\": \"--sessionlbl\", \"command-line-separator\": \" \", \"select-values\": []}], \"outputs\": [{\"name\": \"fixerqc\", \"description\": \"fixerqc\", \"required\": true, \"mount\": \"out\", \"path\": \"Fixer_qc.json\"}], \"xnat\": [{\"name\": \"fixer\", \"label\": \"fixer-v1.1\", \"description\": \"Run DICOM fixer v1.1\", \"contexts\": [\"xnat:imageScanData\"], \"external-inputs\": [{\"name\": \"scan\", \"description\": \"Input scan\", \"type\": \"Scan\", \"required\": true, \"provides-files-for-command-mount\": \"in\", \"load-children\": false}], \"derived-inputs\": [{\"name\": \"session\", \"type\": \"Session\", \"required\": true, \"user-settable\": false, \"load-children\": true, \"derived-from-wrapper-input\": \"scan\", \"multiple\": false}, {\"name\": \"session_label\", \"type\": \"string\", \"required\": true, \"provides-value-for-command-input\": \"session-label\", \"load-children\": true, \"derived-from-wrapper-input\": \"session\", \"derived-from-xnat-object-property\": \"label\", \"multiple\": false}, {\"name\": \"project\", \"type\": \"string\", \"required\": true, \"provides-value-for-command-input\": \"project\", \"load-children\": true, \"derived-from-wrapper-input\": \"session\", \"derived-from-xnat-object-property\": \"project-id\", \"multiple\": false}], \"output-handlers\": [{\"name\": \"fixer\", \"accepts-command-output\": \"fixerqc\", \"as-a-child-of\": \"scan\", \"type\": \"Resource\", \"label\": \"DICOM-Fixer\", \"tags\": []}]}], \"reserve-memory\": 500, \"container-labels\": {}, \"generic-resources\": {}, \"ulimits\": {}, \"secrets\": []}]"