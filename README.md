# DICOM Fixer

This scan-level container can fix problematic DICOM tags and generate a new fixed scan as described below. This corresponds to the `0.9.7` tag of the [DICOM Fixer](https://gitlab.com/flywheel-io/scientific-solutions/gears/dicom-fixer/-/tree/0.9.7?ref_type=tags) gear.

### Release notes

**1.1**
- Corresponds to DICOM Fixer Gear v0.9.7
- **Enhancements**:
     - Added available memory check to catch too-large files before decompression,
so that by default it skips decompression vs being OOM killed.
  - Added `force_decompress` config to force decompression even if the
input may be too big (Expert option)
- Set `strict-validation` default to True
- Updated to `fw-file:3.3.0`

**1.0**
- Initial release corresponding to DICOM Fixer Gear v0.8.3
---

- [DICOM Fixer](#dicom-fixer)
    - [Release notes](#release-notes)
    - [Summary](#summary)
    - [Configs](#configs)
    - [Transfer Syntax](#transfer-syntax)
    - [Fixes](#fixes)
      - [Fix 1: General fixes from fw-file](#fix-1-general-fixes-from-fw-file)
      - [Fix 2: Fixes from fw-file when `strict_validation` is enabled](#fix-2-fixes-from-fw-file-when-strict_validation-is-enabled)
      - [Fix 3: Custom fixes](#fix-3-custom-fixes)
    - [Outputs](#outputs)
    - [Cite](#cite)

### Summary
* It is assumed that DICOM files are stored under a resource called `DICOM` under the scan. Fixer won't proceed without it.
* Fixer comes with `2023d` DICOM standard, which is utilized by the `fw-file` package
* If any fixes are made to the scan, that scan will be reuploaded back (via REST API call using the Import Service) to the same session as a new scan. Those DICOMs will have a new SeriesInstanceUID
* The `unique` option checks the DICOMs for duplicate SOPInstanceUIDs or file hashes. However XNAT by default will catch this and throw an error at the pre-archive when data is originally ingested. This `unique` option is redundant, however it is kept and set to `True` by default just in case.
* `strict_validation` by default is enabled:
  * For situations where information retention is preferred over strict DICOM standard adherance, the `strict_validation` config should be set to `False`. This will prevent Fixer from making any changes to the __values__ of the tags.
  * However Fixer __will still__ attempt to make Fixes 1 and 3 as listed in [Fixes](#fixes) section.
* `strict_validation` when enabled:
  * DICOM tags need to adhere to the DICOM standard `2023d`. Any changes that are made are listed under [Fix 2](#fix-2-fixes-from-fw-file-when-strict_validation-is-enabled). This option __does not make the DICOMs compliant__ to the the DICOM Standard.
  * Fixer __will still__ attempt to make Fixes 1 and 3 
* Performance
  * `reserve-memory` in the `command.json` was set to 500 MB
  * Fixer only requires 1 CPU

### Configs
These are set through the UI:
- *debug*
  - __Description__: *Include debug statements in output.*
  - __Default__: `False`
- *standardize_transfersyntax*
  - __Description__: *Whether or not to change `TransferSyntaxUID` to ExplicitVRLittleEndian.*
  - __Default__: `True`
- *strict_validation*
  - __Description__: *Enforce strict DICOM validation to the DICOM standard*
  - __Default__: `True`
- *unique*
  - __Description__: *Enforce DICOM uniqueness by SOPInstanceUID or file hash. Remove duplicate DICOMs.*
  - __Default__: `True`
- *force_decompress*
   - __Description__: *Expert option: Force standardize_transfer_syntax fix even if filesize may be too big for available memory. WARNING: Choosing this option may result in the container failing due to being out of memory.*
  - __Default__: `False`


### Transfer Syntax

The `standardize_transfersyntax` option supports the following Transfer Syntaxes:

| Name                               | UID                    | Supported          |
|------------------------------------|------------------------|--------------------|
| Explicit VR Little Endian          | 1.2.840.10008.1.2.1    | :white_check_mark: |
| Implicit VR Little Endian          | 1.2.840.10008.1.2      | :white_check_mark: |
| Explicit VR Big Endian             | 1.2.840.10008.1.2.2    | :white_check_mark: |
| Deflated Explicit VR Little Endian | 1.2.840.10008.1.2.1.99 | :white_check_mark: |
| RLE Lossless                       | 1.2.840.10008.1.2.5    | :white_check_mark: |
| JPEG Baseline (Process 1)          | 1.2.840.10008.1.2.4.50 | :white_check_mark: |
| JPEG Extended (Process 2 and 4)    | 1.2.840.10008.1.2.4.51 | :white_check_mark: |
| JPEG Lossless (Process 14)         | 1.2.840.10008.1.2.4.57 | :white_check_mark: |
| JPEG Lossless (Process 14, SV1)    | 1.2.840.10008.1.2.4.70 | :white_check_mark: |
| JPEG LS Lossless                   | 1.2.840.10008.1.2.4.80 | :white_check_mark: |
| JPEG LS Lossy                      | 1.2.840.10008.1.2.4.81 | :white_check_mark: |
| JPEG2000 Lossless                  | 1.2.840.10008.1.2.4.90 | :white_check_mark: |
| JPEG2000                           | 1.2.840.10008.1.2.4.91 | :white_check_mark: |
| JPEG2000 Multi-component Lossless  | 1.2.840.10008.1.2.4.92 | :x:                |
| JPEG2000 Multi-component           | 1.2.840.10008.1.2.4.93 | :x:                |

All transfer syntax decompressing is done with pydicom using either `numpy` and
`GDCM`, `JPEG-LS`, `Pillow`, or `pylibjpeg`,  see more info on pydicom's docs on
[supported transfer syntaxes](https://pydicom.github.io/pydicom/stable/old/image_data_handlers.html)

Additionally the `standardize_transfersyntax` will convert the color space for better downstream support. Currently color space conversion is supported for the following PhotometricInterpretations:

- YBR_FULL_422
- YBR_FULL
- PALETTE COLOR


### Fixes
The following fixes are applied:
#### Fix 1: General fixes from [fw-file](https://gitlab.com/flywheel-io/tools/lib/fw-file) 
- Fix VR for SpecificCharacterSet and surplus SequenceDelimitationItem tags.
- Replace VR=None with VR found in the public or private dictionaries.
- Replace VR='UN' with VR found in the public or private dictionaries.
- Fix invalid VR value. Try to fix an invalid value for the given VR.

#### Fix 2: Fixes from [fw-file](https://gitlab.com/flywheel-io/tools/lib/fw-file) when `strict_validation` is enabled
- Fix an invalid UID. Determine if UID is semi-valid (e.g. composed of a minimum 5 nodes
  including invalid node starting with 0), if semi-valid, generates a new UID with the
  semi-valid UID as entropy source (deterministic), else, generates a new UID.
- Fix date times tags. Attempt to parse an invalid date and format correctly.
- Fix AS strings. Ensure one of D,W,M,Y is at end of the string, and pad to 4
  characters. If no time quantifier is present, assume Years.
- Fix number strings. Fix DS (Decimal String) and IS (Integer String) number strings by
  removing invalid characters and truncate floats in IS VR.
- Fix invalid character. Attempt to remove non-printable characters from byte decoding.
- Fix LUT Descriptor tags.
- Replace invalid \ characters with _ in string VR values of VM=1.
- Crop text VRs which are too long.
#### Fix 3: Custom fixes
- Correct MagneticFieldStrength from milli-Tesla to Tesla. If MagneticFieldStrength is greater than 30, it's likely that it's in milli-Tesla. In this case divide MagneticFieldStrength by 1000.
- Fix invalid Modality. If Modality is empty, set it to `OT`. If the modality isn't in a predefined list (in `fixers.py`), try to find the closest match otherwise set it to `OT`.
- Fix PatientSex so it's set to either `M`, `F`, or `O` (Male, Female, Other). If it's unable to do this, PatientSex will be set as empty.  
- Sets the TransferSyntaxUID to ExplicitVRLittleEndian (if `standardize_transfersyntax` option is enabled)


### Outputs

Fixer will only create a new scan if tags needed fixing. The new scan's DICOMs will differ in `SeriesInstanceUID` and `SOPInstanceUID` from the input scan. The `-{modality}{numeral}` scan ID is automatically assigned by XNAT's merging process.
 * <img src="./images/fixeroutput_scan.png" width=80% />
Fixer will also output a scan resource under the scan that it was run on. This JSON file will list the changes that were made:
 * <img src="./images/fixeroutput_json.png" width=40% />


### Cite

*License*: *MIT*
