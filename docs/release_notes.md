# Release notes

1.1
---
- Corresponds to DICOM Fixer Gear v0.9.7
- **Enhancements**:
    - Added available memory check to catch too-large files before decompression,
so that by default it skips decompression vs being OOM killed.
  - Added `force_decompress` config to force decompression even if the
input may be too big (Expert option)
- Set `strict-validation` default to True
- Updated to `fw-file:3.3.0`

1.0 
---
- Initial release corresponding to DICOM Fixer Gear v0.8.3