"""Parser module to parse gear config.json."""
import os
import sys
import argparse
import logging
import typing as t
from pathlib import Path

import psutil

from fw_file.dicom import get_config
from humanize import naturalsize

from fw_gear_dicom_fixer.utils import calculate_decompressed_size
from fw_gear_dicom_fixer.xnat_logging import stdout_log

parser = argparse.ArgumentParser(description='DICOM Fixer')
parser.add_argument('--input_dir',required=False,default="/input/DICOM")
parser.add_argument('--output_dir',required=False,default="/output")

parser.add_argument('--debug',required=False,default="False")
parser.add_argument('--strict_validation',required=False,default="True")
parser.add_argument('--unique',required=False,default="True")
parser.add_argument('--standardize_transfersyntax',required=False,default="True")
parser.add_argument('--force_decompress',required=False,default="False")

parser.add_argument('--sessionlbl' ,required=True)
parser.add_argument('--projectid' ,required=True)

args=parser.parse_args()

def parse_config() -> t.Tuple[Path, bool, bool, str]:
    """Parse config.json and return relevant inputs and options."""
    input_path = args.input_dir
    output_dir = args.output_dir
    transfer_syntax = True if args.standardize_transfersyntax.lower() == "true" else False
    force_decompress = True if args.force_decompress.lower() == "true" else False
    unique = True if args.unique.lower() == "true" else False
    strict_validation = True if args.strict_validation.lower() == "true" else False

    config = get_config()
    config.reading_validation_mode = (
        "2" if strict_validation == True else "1"
    )
    
    # set logging level
    debug = True if args.debug.lower() == "true" else False
    if not debug:
        stdout_log.setLevel(logging.INFO)
    else:
        stdout_log.debug("Enabling DEBUG level logs")
        stdout_log.setLevel(logging.DEBUG)

    if not os.path.exists(input_path):
        stdout_log.error("%s was not found under this scan. Exiting.",input_path)
        sys.exit(1)

    project=args.projectid
    session_label=args.sessionlbl
    
    configs = {
            "debug": debug,
            "standardize_transfersyntax": transfer_syntax,
            "strict_validation": strict_validation,
            "unique": unique,
            "force_decompress": force_decompress
    }

    # Check memory availability and filesize to catch potential OOM kill
    # on decompression if transfer_syntax == True
    fail_status = False
    if transfer_syntax:
        current_memory = psutil.virtual_memory().used
        decompressed_size = calculate_decompressed_size(input_path)
        total_memory = psutil.virtual_memory().total
        
        if (current_memory + decompressed_size) > (0.7 * total_memory):
            if force_decompress is True:
                stdout_log.warning(
                    "DICOM file may be too large for decompression:\n"
                    f"\tEstimated decompressed size: {naturalsize(decompressed_size)}\n"
                    f"\tCurrent memory usage: {naturalsize(current_memory)}\n"
                    f"\tTotal memory: {naturalsize(total_memory)}\n"
                    "force_decompress is set to True, continuing as configured."
                )
            else:
                stdout_log.warning(
                    "DICOM file may be too large for decompression:\n"
                    f"\tEstimated decompressed size: {naturalsize(decompressed_size)}\n"
                    f"\tCurrent memory usage: {naturalsize(current_memory)}\n"
                    f"\tTotal memory: {naturalsize(total_memory)}\n"
                    "To avoid gear failure due to OOM, standardize_transfer_syntax "
                    "will be switched to False and the DICOM will not be decompressed. "
                    "To force decompression, re-run gear with `force_decompress=True`."
                )
                transfer_syntax = False
                fail_status = True

    return input_path, transfer_syntax, unique, fail_status, output_dir, project, session_label, configs
