#!/usr/bin/env python
"""The run script."""
import sys
import os
import argparse
import json
import traceback
from pathlib import Path
import logging

from fw_file.dicom import get_config
from fw_gear_dicom_fixer import main as dicom_fixer
from fw_gear_dicom_fixer import parser
from fw_gear_dicom_fixer.xnat_logging import stderr_log,stdout_log


def main() -> None:
    """Parse config and run."""

    dicom, transfer_syntax, unique, fail_status,output_dir, project, session_label,configs = parser.parse_config()

    out_fname, events = dicom_fixer.run(
        dicom,
        output_dir,
        transfer_syntax=transfer_syntax,
        unique=unique,
        zip_single="match",
        projectid=project,
        session_label=session_label
    )

    if events is not None:
        stdout_log.info("Changes made: %s",events)


        if fail_status:
            # If fail_status is true here, gear failed at parser size check
            events.update(
                {
                    "Gear Fail": [
                        "File too large for decompression, standardize_transfer_syntax switched to False"
                    ]
                }
            )
       

    if not events:
        events="No changes were made"
    fixer_changes = {
        "fixed":{
            "state":("PASS" if events is not None and not fail_status else "FAIL"),
            "events":events
        },
        "job_info":{"config":configs}
    }
        
    # save the changes
    events_savepath=f"{output_dir}/Fixer_qc.json"
    with open(events_savepath, "w") as json_file:
        json.dump(fixer_changes, json_file)

if __name__ == "__main__":  # pragma: no cover
    try:
        main()
    except Exception as exp:
        traceback_info = traceback.format_exc()
        stdout_log.error("There was an exception: %s Check stderr.log for more details. \n",exp)
        stderr_log.error("Exception: %s %s \n",exp,traceback_info)
        sys.exit(1)

